const ID_REGEX = /[0-9A-F]+$/
const TYPE_REGEX = /\/view\/([a-z-]+)\/[0-9A-F]+$/

module.exports = {
  getId (url) {
    const match = url.match(ID_REGEX)
    return match && match[0]
  },

  getType (url) {
    const match = url.match(TYPE_REGEX)
    return match ? match[1] : 'account'
  }
}
