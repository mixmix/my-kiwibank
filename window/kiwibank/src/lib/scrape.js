function isDollars (str) {
  return str.match(/^-?\$/)
}
function toNumber (str) {
  return Number(str.replace(/\$/, '').replace(/,/g, ''))
  // TODO use cents (floats are not safe representaions for money!)
}

// Pattern = {
//   selector: String,
//   output: String | Pattern | [Pattern]
// }
//
// selector defines how to get nextEl(s)
//   - note plural if output is an Array
// output defines what to do with that nextEl
//
// if output is type:
//   - String, return that named attribute on nextEl
//   - Pattern (Object), recurse `scrape(nextPattern, nextEl)`
//   - [Pattern] (Array), recurse `nextEls.map(el => scrape(nextPattern, el))`

// Abbrev
//
// pattern = '.title
//         = { selector: '.title', output: 'innerText' }
//

module.exports = function scrape (parentEl, pattern) {
  if (!pattern) return scrape(document.body, pattern)

  if (typeof pattern === 'string') {
    pattern = {
      selector: pattern,
      output: 'innerText'
    }
  }

  // if there is no selector, continue on down
  if (!pattern.selector) {
    const output = {}
    for (const key in pattern) {
      output[key] = scrape(parentEl, pattern[key])
    }
    return output
  }

  if (!pattern.output) {
    pattern.output = 'innerText'
  }
  // |
  // +- Leaf       // String
  // +- [Node]     // Array
  // +- Node       // Object

  // if output is a string, this is an a Leaf!
  if (typeof pattern.output === 'string') {
    const nextEl = parentEl.querySelector(pattern.selector)
    if (!nextEl) return null

    const value = nextEl[pattern.output] || nextEl.getAttribute(pattern.output)
    return isDollars(value) ? toNumber(value) : value
  } // eslint-disable-line
  else if (Array.isArray(pattern.output)) {
    return Array.from(parentEl.querySelectorAll(pattern.selector)).map(
      (nextEl) => {
        return scrape(nextEl, pattern.output[0])
      }
    )
  } // eslint-disable-line
  else if (typeof pattern.output === 'object') {
    const nextEl = parentEl.querySelector(pattern.selector)
    if (!nextEl) return null

    const output = {}
    for (const key in pattern.output) {
      if (key === 'selector') continue // should not be selector in output
      output[key] = scrape(nextEl, pattern[key])
    }
    return output
  }
}
