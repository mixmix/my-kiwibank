const scrape = require('../lib/scrape')
const { getId, getType } = require('../lib/url-parser')

module.exports = function scrapeAccounts () {
  return scrape(document.body, {
    selector: '#account_list tr[id]:not(.totals_row)',
    output: [
      {
        url: {
          selector: '.account_title > a',
          output: 'href'
        },
        number: '.account_number',
        holder: '.account_name',
        balance: '.amount:not(.available)',
        available: '.amount.available',
        alias: '.account_title > a'
      }
    ]
  })
    .map(account => {
      account.id = getId(account.url)
      account.type = getType(account.url)
      return account
    })
}
