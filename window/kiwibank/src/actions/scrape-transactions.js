/* eslint-disable brace-style */
const scrape = require('../lib/scrape')

const PAY = 'PAY' // to another persons account
const TRANSFER = 'TRANSFER' // to another account you own
const POS = 'POS' // point of sale (shop) transaction
const POSREJ = 'POSREJ' // point of sale (shop) rejection
const AP = 'AP' // automatic payment
const SP = 'SP' // shopify payment
const DIRECT_DEBIT = 'DIRECT DEBIT'
const SALARY = 'SALARY'
const INTEREST = 'INTEREST'
const IRD = 'IRD' // tax payment
const BILL_PAYMENT = 'BILL PAYMENT'
const GOOGLE = 'GOOGLE' // google pay

// The structure of the pages is alternating rows of:
// 1. transaction summary
// 2. transaction detail
//
// The summary row is always the same shape, though some of the divs are
// overloaded and need parsing
//
// The detail section can be radically different depending on the type of
// transaction.

// TODO: There's a .hide class on a div at the bottom of the page, which shows upcoming automatic payments (.ctl00_c_ProjectedUpcomingPaymentsControl_ProjectedPayments)
// this throws off the (detailEls.length !== summaries.length) comparison, we should do something about it.

module.exports = function scrapeTransactions () {
  const summaries = scrape(document.body, {
    selector: '#TransactionTable tr.details',
    output: [
      {
        date: 'td:nth-child(1)',
        title: 'td:nth-child(2)',
        deposit: 'td:nth-child(3)',
        withdrawal: 'td:nth-child(4)',
        balance: 'td:nth-child(5)'
      }
    ]
  })

  // grab the detail elements, in prep for customised scraping
  const detailEls = document.querySelectorAll('.transaction_details')

  const transactions = summaries.map((summary, index) => {
    const { type, ...other } = parseTitle(summary.title)

    const transaction = {
      type,
      amount:
        typeof summary.deposit === 'number'
          ? summary.deposit
          : -summary.withdrawal,
      balance: summary.balance,
      date: new Date(summary.date.replace(/'/, '')),
      ...other
    }

    const scrapeDetails = getScraperByType(type)
    if (!scrapeDetails) {
      console.warn(`need detailsGetter for type ${type}`)
      return transaction
    }

    const detailEl = detailEls[index]
    if (!detailEl) {
      console.error('couldn not find details for transaction', transaction)
      return transaction
    }

    return {
      ...transaction,
      ...scrapeDetails(detailEl)
    }
  })

  // console.log(JSON.stringify(transactions, null, 2))

  return transactions
}

// Kiwibank overloads the "title" field with heaps of info.
// This function takes the "title" and returns { type, ...other } where other is a
// collection of misc. key-value pairs
function parseTitle (title) {
  title = title.trim()

  // pay to another person/org
  if (title.startsWith(PAY)) {
    return {
      type: PAY,
      payee: title.replace('PAY ', '')
    }
  }

  // transfer between accounts
  if (title.startsWith(TRANSFER)) {
    return {
      type: TRANSFER,
      title: title.replace('TRANSFER ', '')
    }
  }

  // // point of sale (used a card) & point of sale rejected
  if (title.startsWith(POSREJ)) {
    const timeMatch = title.match(/\d\d:\d\d/, '')

    return {
      type: POSREJ,
      reason: title
        .replace('POSREJ ', '')
        .replace(/-\d\d:\d\d$/, ''),
      time: timeMatch ? timeMatch[0] : null
    }
  }

  if (title.startsWith(POS)) {
    const timeMatch = title.match(/\d\d:\d\d/, '')

    return {
      type: POS,
      vendor: title
        .replace('POS W/D ', '')
        .replace(/-\d\d:\d\d$/, '')
        .replace(conversionRegex, '')
        .replace(commissionRegex, ''),
      time: timeMatch ? timeMatch[0] : null,

      ...getCurrencyDetails(title)
    }
  }

  // automatic payment
  if (title.startsWith(AP)) {
    return {
      type: AP,
      id: title.replace(/\D/g, '')
    }
  }

  // Shopify Payment
  if (title.startsWith(SP)) {
    return {
      type: SP,
      vendor: title.replace('SP ', '')
    }
  }
  // Google Pay
  if (title.startsWith(GOOGLE)) {
    return {
      type: SP,
      vendor: title.replace('GOOGLE ', '')
    }
  }

  // direct debit
  if (title.startsWith(DIRECT_DEBIT)) {
    return {
      type: DIRECT_DEBIT,
      payee: title.replace('DIRECT DEBIT -', '')
    }
  }

  // salary
  if (title.startsWith(SALARY)) {
    return {
      type: SALARY,
      location: title.replace('SALARY ', '')
    }
  }

  // tax payments
  if (title.startsWith(IRD)) {
    const rateMatch = title.match(/\d+.?\d*%$/, '')
    return {
      type: IRD,
      taxType: title
        .replace('IRD ', '')
        .replace(/ \d+.?\d*%$/, ''),
      taxRate: rateMatch ? parseFloat(rateMatch[0].replace('%', '')) : null
    }
  }

  if (title.startsWith(INTEREST)) {
    return {
      type: INTEREST,
      title: title.replace('INTEREST ', '')
    }
  }

  if (title.startsWith(BILL_PAYMENT)) {
    return {
      type: BILL_PAYMENT,
      title: title.replace('BILL PAYMENT ', '')
    }
  }

  return {
    type: 'OTHER',
    otherTitle: title
  }
}

// international transactions have all the currency conversion details in the title
const conversionRegex = /(\d*.?\d*)([A-Z]{3}) @ (\d*.?\d*) CONVERSION RATE/
const commissionRegex = /\(INC. (\$\d*.?\d*) CURRENCY CONVERSION COMMISSION\) /
function getCurrencyDetails (title) {
  if (!title.match('CONVERSION RATE')) return null

  const commissionAmount = title.match(commissionRegex)[1].replace('$', '')
  const [, originalAmount, originalCurrency, conversionRate] = title.match(conversionRegex)

  return {
    commissionAmount: Number(commissionAmount),
    originalAmount: Number(originalAmount),
    originalCurrency,
    conversionRate: Number(conversionRate)
  }
}

function getScraperByType (type) {
  switch (type) {
    case POS:
    case POSREJ:
    case SP:
    case GOOGLE:
      return scrapePOS

    case AP:
    case TRANSFER:
    case PAY:
    case BILL_PAYMENT:
    case DIRECT_DEBIT:
    case SALARY:
    case IRD:
    case INTEREST:
      return scrapeTransfer

    case 'OTHER':
      return scrapeOther
  }
}

//
// Details scapers for each transaction.type
//

const accountNumberRegex = /\d{2}-\d{4}-\d{7}-\d{2}/
function scrapeTransfer (detailEl) {
  const statementDetails = scrapeStatement(detailEl)

  const hasAccountDetails = Array.from(detailEl.querySelectorAll('dt'))
    .some(el => el.innerText === 'Their Account:')

  let detail
  if (hasAccountDetails) {
    detail = scrape(detailEl, {
      description: 'dd:nth-of-type(1)',
      theirAccountDetails: 'dd:nth-of-type(2)',
      amount: 'dd:nth-of-type(3)',
      ref: 'dd:nth-of-type(4)'
    })
    detail.theirAccount = detail.theirAccountDetails.match(accountNumberRegex)[0]
    detail.theirName = detail.theirAccountDetails.replace(accountNumberRegex, '')
    delete detail.theirAccountDetails
  }
  else {
    detail = scrape(detailEl, {
      description: 'dd:nth-of-type(1)',
      amount: 'dd:nth-of-type(2)',
      ref: 'dd:nth-of-type(3)'
    })
  }

  return {
    ...statementDetails,
    ...detail
  }
}

function scrapePOS (detailEl) {
  const hasEffectiveDate =
    detailEl.querySelector('dt.first').innerText === 'EffectiveDate:'

  const fields = hasEffectiveDate
    ? {
        effectiveDate: 'dd:nth-of-type(1)',
        description: 'dd:nth-of-type(2)',
        amount: 'dd:nth-of-type(3)',
        cardNumber: 'dd:nth-of-type(4)',
        ref: 'dd:nth-of-type(5)'
      }
    : {
        description: 'dd:nth-of-type(1)',
        amount: 'dd:nth-of-type(2)',
        cardNumber: 'dd:nth-of-type(3)',
        ref: 'dd:nth-of-type(4)'
      }

  const details = scrape(detailEl, fields)
  if (hasEffectiveDate) {
    details.effectiveDate = new Date(details.effectiveDate.replace(/'/, ''))
  }

  return details
}

function scrapeOther (detailEl) {
  const fieldNames = Array.from(detailEl.querySelectorAll('dt'))
    .map(el => camelCase(el.innerText.replace(':', '')))

  const fieldValues = Array.from(detailEl.querySelectorAll('dd'))
    .map(el => el.innerText)

  return fieldNames.reduce((acc, fieldName, i) => {
    if (fieldName === 'effectiveDate') {
      acc[fieldName] = new Date(fieldValues[i].replace(/'/, ''))
    }
    else if (fieldName === 'amount') {
      acc[fieldName] = Number(fieldValues[i].replace('$', ''))
    }
    else {
      acc[fieldName] = fieldValues[i]
    }

    return acc
  }, {})
}

function scrapeStatement (detailEl) {
  const statementDetailsEl = detailEl.querySelector('.statement_details')
  if (!statementDetailsEl) return null

  const yourDetailsEl = statementDetailsEl.querySelector('tr:nth-child(2)')
  const theirDetailsEl = statementDetailsEl.querySelector('tr:nth-child(3)')

  const pattern = {
    particulars: 'td:nth-of-type(2)',
    code: 'td:nth-of-type(3)',
    ref: 'td:nth-of-type(4)'
  }

  return {
    yourStatement: scrape(yourDetailsEl, pattern),
    theirStatement: scrape(theirDetailsEl, pattern)
  }
}

// Mix: I wrote this because I was having trouble importing an installed one!
function camelCase (string) {
  return string.match(/[A-Z]?[a-z]+/g)
    .map((str, i) => {
      if (i === 0) return str.toLowerCase()
      return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase()
    })
    .join('')
}
