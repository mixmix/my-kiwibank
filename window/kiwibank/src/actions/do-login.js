const { ipcRenderer } = require('electron')

module.exports = async function login () {
  const {
    KIWIBANK_ACCESS_NUMBER,
    KIWIBANK_PASSWORD
  } = await ipcRenderer.invoke('getENV')

  if (!KIWIBANK_ACCESS_NUMBER || !KIWIBANK_PASSWORD) return

  const getEl = (selector) => document.body.querySelector(selector)

  getEl('#username_input > input').value = KIWIBANK_ACCESS_NUMBER
  getEl('#password_input > input').value = KIWIBANK_PASSWORD
  getEl('#FinalStepButtonDiv input').click()
}
