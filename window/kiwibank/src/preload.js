/* eslint-disable brace-style */

// This is the preload script that is installed by electron
// into kiwibank.co.nz pages each time they're loaded in this window

const { ipcRenderer } = require('electron')

const { doLogin, scrapeAccounts, scrapeTransactions } = require('./actions')
const { getId } = require('./lib/url-parser')

const USER_LOGIN = 'https://www.ib.kiwibank.co.nz/login/'
const ACCOUNT_INDEX = 'https://www.ib.kiwibank.co.nz/accounts/'

// We request that the main process sends us a channel we can use to
// communicate with the backend
let port
ipcRenderer.send('request-backend-channel')
ipcRenderer.once('provide-backend-channel', (event) => {
  port = event.ports[0]
  // ... register a handler to receive results ...
  port.onmessage = (event) => {
    console.log('received from backend:', event.data)
    // WIP listen to the backend telling us to go check out another account page
    // from the backend just every 1min puppet this page to go somewhere else
    // and send back updates
    //
    // if (event.data.type === 'scrape') {
    //   window.location.href = event.data.payload
    // }
    if (typeof event.data === 'string') {
      window.location.href = event.data
    }
  }
})

window.addEventListener('DOMContentLoaded', () => {
  /* Login Page */
  if (window.location.href === USER_LOGIN) {
    ipcRenderer.send('login-ready')
    doLogin()
  }

  /* Account Index */
  else if (window.location.href === ACCOUNT_INDEX) {
    ipcRenderer.send('login-complete')

    transmit({
      type: 'accounts',
      payload: scrapeAccounts()
    })
  }

  /* Account Show */
  else if (window.location.pathname.startsWith('/accounts/view/')) {
    transmit({
      type: 'transactions',
      payload: {
        id: getId(window.location.href),
        transactions: scrapeTransactions()
      }
    })
  }
})

function transmit (msg) {
  if (!port) return setTimeout(() => transmit(msg), 100)
  // TODO handle port never being ready?

  port.postMessage(msg)
}
