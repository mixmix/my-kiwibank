const { BrowserWindow } = require('electron')
const path = require('path')

module.exports = function startKiwibank () {
  // Create the browser window.
  const kiwibank = new BrowserWindow({
    width: 800,
    height: 600,
    frame: false,
    titleBarStyle: 'hidden',
    webPreferences: {
      preload: path.join(__dirname, 'dist/preload.js')
    }
  })
  kiwibank.loadURL('https://www.ib.kiwibank.co.nz/login/')

  // Open the DevTools.
  kiwibank.webContents.openDevTools()

  return kiwibank
}
