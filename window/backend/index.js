const { BrowserWindow } = require('electron')
const path = require('path')

module.exports = function startBackend () {
  // Create the browser window.
  const kiwibank = new BrowserWindow({
    width: 800,
    height: 600,
    // show: false, // comment out for debugging!
    webPreferences: {
      preload: path.join(__dirname, 'dist/index.js'),
      nodeIntegration: true // sandbox: false
    }
  })

  // and load the index.html of the app.
  kiwibank.loadFile(path.join(__dirname, 'index.html'))

  // Open the DevTools.
  kiwibank.webContents.openDevTools()

  return kiwibank
}
