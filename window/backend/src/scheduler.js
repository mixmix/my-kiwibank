const MINUTE = 60_000

module.exports = class Scheduler {
  #handler
  // somewhere in here, start an interval where we re-query the accounts
  // TODO may need a way to clearInterval on electron close

  updateAccounts (payload) {
    // check each account for update of balalnce
    // if new update in balance,
    //  - queue up new accountTransactions scrape
    //  - update local state
    // store the balance for each account only
    setTimeout(() => {
      this.#handler('https://www.ib.kiwibank.co.nz/accounts/')
    }, 1 * MINUTE)
  }

  onLocationRequest (handler) {
    this.#handler = handler
    // only need to store one handler
  }
}
