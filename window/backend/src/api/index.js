const http = require('http')
const cors = require('cors')
const express = require('express')
const { createYoga, createSchema } = require('graphql-yoga')

const typeDefs = require('./typeDefs')
const resolvers = require('./resolvers')

module.exports = function API (store, opts = {}) {
  const {
    port = 9001
  } = opts

  const app = express()
  app.use(cors())
  app.use(
    '/graphql',
    createYoga({
      schema: createSchema({
        typeDefs,
        resolvers
      }),
      context: (req) => ({ store }),
      // Context factory gets called for every request
      graphiql: true
    })
  )

  const httpServer = http.createServer(app)

  httpServer.listen(port, (err) => {
    if (err) {
      console.error(err)
      // TODO do something better
    }

    console.log(`listening on http://localhost:${port}/graphql`)
  })
}
