const gql = require('graphql-tag')

module.exports = gql`
  type Query {
    getAccounts: [Account]! 

    getAccount (id: ID!): Account
  }

  type Account {
    id: ID!
    type: String!
    number: String!
    holder: String!
    alias: String
    available: Float!
    balance: Float!
    url: String!

    transactions: [Transaction]
  }

  type Transaction {
    type: String
    amount: Float!
    balance: Float!
    description: String
    meta: [TransactionExtra]
  }

  type TransactionExtra {
    key: String!
    value: String
  }
`
