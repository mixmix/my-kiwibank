module.exports = {
  Query: {
    getAccounts (_, opts, { store }) {
      return store.getAccounts()
    },

    getAccount (_, opts, { store }) {
      const { id } = opts

      return store.getAccounts().find(account => account.id === id)
    }
  },
  Account: {
    transactions (account, _, { store }) {
      return store.getAccountTransactions(account.id).map(t => {
        const { type, amount, balance, description, ...meta } = t

        return {
          type,
          amount,
          balance,
          description,
          meta: Object.entries(meta)
            .map(([key, value]) => ({ key, value: value && value.toString() }))
          // TODO handle case of Object
          // theirTransaction: [Object]
          // yourStatement: [Object]
          // not sure if this is a good solution...
        }
      })
    }
  }
}
