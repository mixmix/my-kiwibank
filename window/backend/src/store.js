module.exports = class Store {
  #accounts = []
  #accountTransactions = {
    // id: [transaction]
  }

  getAccounts () { return this.#accounts }
  setAccounts (data) { this.#accounts = data }

  getAccountTransactions (id) { return this.#accountTransactions[id] }
  setAccountTransactions (id, data) { this.#accountTransactions[id] = data }
}
