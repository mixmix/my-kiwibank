/* eslint-disable brace-style */
const { ipcRenderer } = require('electron')

const Store = require('./store')
const API = require('./api')
const Scheduler = require('./scheduler')

const store = new Store()
const scheduler = new Scheduler(store)
API(store, { port: 9001 })

// wait for channel set up by kiwibank window
ipcRenderer.on('provide-kiwibank-channel', (event) => {
  const [port] = event.ports

  // listen for updates from the scraper to update the state
  port.onmessage = (ev) => {
    console.log('update received')
    const { type, payload } = ev.data

    if (type === 'accounts') {
      store.setAccounts(payload)
      scheduler.updateAccounts(payload)
    }
    else if (type === 'transactions') {
      store.setAccountTransactions(payload.id, payload.transactions)
    }
  }

  scheduler.onLocationRequest(location => {
    port.postMessage(location)
  })
})
