# my-kiwibank


## Project Structure

The `Main` electron process spawns 3 windows:
1. `Kiwibank`
  - the actual kiwibank website
  - our custom `preload` script is also loaded, allowing us to "puppet" the website
  - this window is hidden after login is completed, but remains active
2. `Backend`
  - hosts a `store`, our local record of all account + transaction state
  - hosts a graphQL `api` which draws data from the `store`
  - hosts a `scheduler` which puppets new updates out of the `Kiwibank` window
3. `UI`
  - a Vue app which makes graphQL calls to the api for data.

```mermaid
flowchart LR

main[Main]
main -.- windowKiwibank
main -.- windowApi
main -.- windowUi

subgraph windowKiwibank[Kiwibank]
  kiwibank.co.nz:::green
  preload

  kiwibank.co.nz --scrape--> preload
end 

subgraph windowApi[Backend]
  store
  scheduler
  api

  store -.- api
  store -.- scheduler
end

subgraph windowUi[UI]
  ui[app.js]
end

preload --update--> store
scheduler --location--> preload
ui <---> api

classDef default fill:#fff, stroke:#19e480, stroke-width:2;

classDef green color:#fff, fill:#19e480, stroke: none;
classDef noFill fill:none, stroke: none;
classDef whiteFill fill:#fff, stroke: none;

classDef cluster color:#19e480, stroke:#19e480, fill:#efe;


class windowKiwibank,windowApi,windowUi greenOutline;
```

NOTES:
- communication between the `Kiwibank` and `Backend` windows is mediated via a MessageChannel
- the `preload` script:
  - is loaded each time the page is loaded
  - performs set actions given its location (e.g. accounts page => `scrapeAccounts()`)
  - listens for location change requests and executes them (this stimulates new scrapes)

## Development

Install dependencies

```bash
npm i
```

Live rebuild the kiwibank scraper script
```bash
npm run scraper:watch
```

Live rebuilt the backend (db + api)
```bash
npm run backend:watch
```

Start up electron
```bash
npm start
```

The graphQL server can be found at http://localhost:9001/graphql


