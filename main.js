const { app, BrowserWindow, MessageChannelMain, ipcMain } = require('electron')

require('dotenv').config()

const startBackend = require('./window/backend/index.js')
const startKiwibank = require('./window/kiwibank/index.js')
// NOTE we have to specify index.js because the package.json main
// is used to configure Pracel output, which is not what we want

ipcMain.handle('getENV', () => {
  return {
    KIWIBANK_ACCESS_NUMBER: process.env.KIWIBANK_ACCESS_NUMBER,
    KIWIBANK_PASSWORD: process.env.KIWIBANK_PASSWORD
  }
})

app.whenReady().then(() => {
  const backend = startBackend()
  const kiwibank = startKiwibank()

  ipcMain.on('request-backend-channel', (event) => {
    if (event.senderFrame !== kiwibank.webContents.mainFrame) return
    // only the frames we expect can access the the backend

    const { port1, port2 } = new MessageChannelMain()
    backend.webContents.postMessage('provide-kiwibank-channel', null, [port1])
    kiwibank.webContents.postMessage('provide-backend-channel', null, [port2])
  })

  ipcMain.on('login-ready', () => kiwibank.show())
  // ipcMain.on('login-complete', () => kiwibank.hide())
  // TODO uncomment once have a healthy cycling of updates

  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) startKiwibank()
  })
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})
